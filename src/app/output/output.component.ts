import { Component, OnInit, Input, Output,  EventEmitter } from '@angular/core';

@Component({
  selector: 'app-output',
  templateUrl: './output.component.html',
  styleUrls: ['./output.component.css']
})
export class OutputComponent implements OnInit {

  @Input('namepass') namelocal:any;


  constructor() { }

  ngOnInit() {
  }

  @Output() event =new EventEmitter();

  // func()
  // {
  //   this.event.emit('kc');
  // }


  childToParent()
  {
    this.event.emit('kc');
  }


}
