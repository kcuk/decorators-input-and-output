import {Routes, RouterModule} from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { OutputComponent } from './output.component';

const routes: Routes =[
    {path:'', component:OutputComponent},
    {path:'output', component:OutputComponent}
]

export const outputmwp: ModuleWithProviders = RouterModule.forChild(routes);