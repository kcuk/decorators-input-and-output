import {NgModule} from '@angular/core';

import { outputmwp } from './output.routing';
import { OutputComponent } from './output.component';

@NgModule(
    {
        declarations:[OutputComponent],
        imports:[outputmwp],
        providers:[],
        bootstrap:[],
        exports:[OutputComponent]
    }
)

export class OutputModule { }