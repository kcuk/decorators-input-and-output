import {Routes,RouterModule} from '@angular/router';
import { ModuleWithProviders } from '@angular/core';

const routes: Routes =[
    {
        path:'input',loadChildren:'./input/input.module#InputModule'
    },
    {
        path:'output',loadChildren:'./output/output.module#OutputModule'
    }
]

export const mwp: ModuleWithProviders = RouterModule.forRoot(routes);