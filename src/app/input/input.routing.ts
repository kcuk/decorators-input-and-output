import {Routes,RouterModule} from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { InputComponent } from './input.component';

const routes : Routes= [
    {path:'',component:InputComponent},
    {path:'input',component:InputComponent}
]

export const inputmwp:ModuleWithProviders  = RouterModule.forChild(routes);


