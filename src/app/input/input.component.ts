import { Component, OnInit } from '@angular/core';
import { FormGroup,FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.css']
})
export class InputComponent implements OnInit {

  inputfg: FormGroup;
  name:any;
  names:string;
  
  
  constructor(fb:FormBuilder ) {
    this.inputfg = fb.group(
      {
        formname:['',[Validators.required,Validators.minLength(1)]]
      }
    )
   }



  ngOnInit() {
  }

  submit()
  {
    this.name=this.inputfg.controls['formname'].value;
    console.log(this.inputfg.controls['formname'].value);
  }

  childToParent(event)
  {
    console.log(event);
    this.names=event;
  }
}
