import {NgModule} from '@angular/core';
import {inputmwp} from './input.routing';

import {FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InputComponent } from './input.component';
import {OutputModule} from '../output/output.module';



@NgModule(
    {
        declarations:[InputComponent],
        imports:[inputmwp, FormsModule,ReactiveFormsModule, OutputModule],
        providers:[],
        bootstrap:[],
        exports:[]
    }
)

export class InputModule { }